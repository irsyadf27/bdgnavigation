package id.ac.unikom.bdgnavigation.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import id.ac.unikom.bdgnavigation.R;
import id.ac.unikom.bdgnavigation.TerdekatActivity;
import id.ac.unikom.bdgnavigation.database.DBBookmark;

//Class Adapter ini Digunakan Untuk Mengatur Bagaimana Data akan Ditampilkan
public class BookmarkAdapter extends RecyclerView.Adapter<BookmarkAdapter.ViewHolder>{
    private ArrayList placeidList;
    private ArrayList namaList;
    private ArrayList categoryList;
    private ArrayList ratingList;
    private ArrayList vicinityList;

    //Membuat Konstruktor pada Class BookmarkAdapter
    public BookmarkAdapter(ArrayList placeidList, ArrayList namaList, ArrayList categoryList, ArrayList ratingList, ArrayList vicinityList){
        this.placeidList = placeidList;
        this.namaList = namaList;
        this.categoryList = categoryList;
        this.ratingList = ratingList;
        this.vicinityList = vicinityList;
    }

    //ViewHolder Digunakan Untuk Menyimpan Referensi Dari View-View
    class ViewHolder extends RecyclerView.ViewHolder{

        private TextView Nama, Category;
        private RatingBar Rating;
        private Button mButton;

        ViewHolder(View itemView) {
            super(itemView);
            //Menginisialisasi View-View untuk kita gunakan pada RecyclerView
            Nama = itemView.findViewById(R.id.nama);
            Category = itemView.findViewById(R.id.textCategory);
            mButton = itemView.findViewById(R.id.btnMenuBookmark);
            Rating = itemView.findViewById(R.id.ratingBar);
            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Log.d("item", "clicked");
                    // get position
                    int pos = getAdapterPosition();

                    // check if item still exists
                    if(pos != RecyclerView.NO_POSITION){
                        final String Placeid = placeidList.get(pos).toString();
                        final String Nama = namaList.get(pos).toString();
                        Context context = v.getContext();
                        Intent myIntent = new Intent(context, MainActivity.class);
                        myIntent.putExtra("place_id", Placeid);
                        myIntent.putExtra("nama_tempat", Nama);
                        context.startActivity(myIntent);
                    }
                }
            });*/
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Membuat View untuk Menyiapkan dan Memasang Layout yang Akan digunakan pada RecyclerView
        View V = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_bookmark, parent, false);
        return new ViewHolder(V);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        //Memanggil Nilai/Value Pada View-View Yang Telah Dibuat pada Posisi Tertentu
        final String Placeid = placeidList.get(position).toString();
        final String Nama = namaList.get(position).toString();
        final String Category = categoryList.get(position).toString();
        final Float Rating = Float.parseFloat(ratingList.get(position).toString());
        final String Vicinity = vicinityList.get(position).toString();

        holder.Nama.setText(Nama);
        holder.Category.setText(Category);
        holder.Rating.setRating(Rating);

        /*holder.Nama.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   Context context = v.getContext();
                   Intent myIntent = new Intent(context, MainActivity.class);
                   myIntent.putExtra("place_id", Placeid);
                   myIntent.putExtra("nama_tempat", Nama);
                   context.startActivity(myIntent);
               }
        });*/


        holder.mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
                popupMenu.inflate(R.menu.popup_menu);
                final View v = view;
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.delete_bookmark:
                                //Menghapus Data Dari Database
                                DBBookmark getDatabase = new DBBookmark(v.getContext());
                                SQLiteDatabase DeleteData = getDatabase.getWritableDatabase();
                                String whereClause = "place_id=?";
                                String[] selectionArgs = {Placeid};
                                DeleteData.delete("Bookmark", whereClause, selectionArgs);

                                //Menghapus Data pada List dari Posisi Tertentu
                                int position = placeidList.indexOf(Placeid);
                                placeidList.remove(position);
                                namaList.remove(position);
                                notifyItemRemoved(position);
                                Toast.makeText(v.getContext(),"Data Dihapus",Toast.LENGTH_SHORT).show();
                                break;
                            case R.id.navigasi_bookmark:
                                Context context = v.getContext();
                                Intent myIntent = new Intent(context, TerdekatActivity.class);
                                myIntent.putExtra("action", "navigation");
                                myIntent.putExtra("place_id", Placeid);
                                myIntent.putExtra("nama_tempat", Nama);
                                myIntent.putExtra("type_tempat", Category);
                                if(Rating != null) {
                                    myIntent.putExtra("rating_tempat", Rating);
                                } else {
                                    myIntent.putExtra("rating_tempat", 0.0);
                                }
                                myIntent.putExtra("alamat_tempat", Vicinity);
                                context.startActivity(myIntent);
                                break;
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        //Menghitung Ukuran/Jumlah Data Yang Akan Ditampilkan Pada RecyclerView
        return namaList.size();
    }
}
