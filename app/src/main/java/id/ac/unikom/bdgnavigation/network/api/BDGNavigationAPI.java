package id.ac.unikom.bdgnavigation.network.api;

import id.ac.unikom.bdgnavigation.model.BDGNavigationApiResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface BDGNavigationAPI {
    @GET("maps/{place_id}")
    Call<BDGNavigationApiResponse> getDescription(@Path("place_id") String place_id);
}
