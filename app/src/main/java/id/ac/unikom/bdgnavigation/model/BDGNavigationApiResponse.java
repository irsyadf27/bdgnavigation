package id.ac.unikom.bdgnavigation.model;

import com.google.gson.annotations.Expose;

public class BDGNavigationApiResponse {
    @Expose
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
