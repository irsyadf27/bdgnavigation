package id.ac.unikom.bdgnavigation.model;

import com.google.gson.annotations.Expose;

public class LegDuration {
    //@SerializedName("value")
    @Expose
    private String text;

    //@SerializedName("value")
    @Expose
    private String value;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
