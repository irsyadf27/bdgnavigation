package id.ac.unikom.bdgnavigation.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class DBBookmark extends SQLiteOpenHelper {
    //InnerClass, untuk mengatur artibut seperti Nama Tabel, nama-nama kolom dan Query
    static abstract class MyColumns implements BaseColumns{
        //Menentukan Nama Table dan Kolom
        static final String NamaTabel = "Bookmark";
        static final String PlaceId = "place_id";
        static final String Nama = "nama";
        static final String Category = "category";
        static final String Rating = "rating";
        static final String Vicinity = "vicinity";
    }

    private static final String NamaDatabse = "bookmark.db";
    private static final int VersiDatabase = 1;

    //Query yang digunakan untuk membuat Tabel
    private static final String SQL_CREATE_ENTRIES = "" +
            "CREATE TABLE " + MyColumns.NamaTabel + "(" +
            MyColumns.PlaceId + " TEXT PRIMARY KEY, " +
            MyColumns.Nama + " TEXT NOT NULL, " +
            MyColumns.Category + " TEXT NOT NULL, " +
            MyColumns.Rating + " REAL NULL, " +
            MyColumns.Vicinity + " TEXT NOT NULL)";

    //Query yang digunakan untuk mengupgrade Tabel
    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + MyColumns.NamaTabel;

    public DBBookmark(Context context) {
        super(context, NamaDatabse, null, VersiDatabase);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
}
