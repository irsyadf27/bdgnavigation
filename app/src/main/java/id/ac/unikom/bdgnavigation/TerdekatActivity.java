package id.ac.unikom.bdgnavigation;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.ac.unikom.bdgnavigation.adapter.OpeningHoursAdapter;
import id.ac.unikom.bdgnavigation.adapter.SearchPlaceAdapter;
import id.ac.unikom.bdgnavigation.custombottomsheet.BottomSheetBehaviorGoogleMapsLike;
import id.ac.unikom.bdgnavigation.custombottomsheet.MergedAppBarLayout;
import id.ac.unikom.bdgnavigation.custombottomsheet.MergedAppBarLayoutBehavior;
import id.ac.unikom.bdgnavigation.database.DBBookmark;
import id.ac.unikom.bdgnavigation.model.BDGNavigationApiResponse;
import id.ac.unikom.bdgnavigation.model.DetailResponse;
import id.ac.unikom.bdgnavigation.model.DirectionApiResponse;
import id.ac.unikom.bdgnavigation.model.NearByApiResponse;
import id.ac.unikom.bdgnavigation.model.Result;
import id.ac.unikom.bdgnavigation.network.api.BDGNavigationAPI;
import id.ac.unikom.bdgnavigation.network.api.GoogleAPI;
import id.ac.unikom.bdgnavigation.network.service.BDGNavigationService;
import id.ac.unikom.bdgnavigation.network.service.GoogleService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TerdekatActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,LocationListener {

    // Kamus data
    private static final String TAG = MainActivity.class.getSimpleName();
    MergedAppBarLayoutBehavior mergedAppBarLayoutBehavior;
    BottomSheetBehaviorGoogleMapsLike behavior;
    private GoogleMap googleMap;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location location;
    private int PROXIMITY_RADIUS = 500; // 500 m
    GoogleAPI mApiInterface;
    GoogleService ApiClient;
    List<LatLng> latLngs;
    PolylineOptions rectOptions;
    Polyline polyline;
    DirectionApiResponse DirectionAnswer;
    Result obj;
    ProgressDialog progressBar;
    private DBBookmark dbBookmark;
    private String place_id;
    private String nama_tempat;
    private String type, type2, aksi;

    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;

    // data result
    private List<Result> data_result;
    private SearchPlaceAdapter searchAdapter;
    private SearchView.SearchAutoComplete searchAutoComplete;

    private BDGNavigationAPI BDGNavigationAPIInterface;
    private BDGNavigationService BDGNavigationAPIClient;
    private BDGNavigationApiResponse BDGNavigationResponse;

    private String type_tempat;
    private Float rating_tempat;
    private String alamat_tempat;

    @BindView(R.id.bottom_sheet_sheet_peek)
    LinearLayout bottomSheetLayout;

    @BindView(R.id.gapTambahan)
    LinearLayout gapTambahan;

    //@BindView(R.id.imgTempat)
    //ImageView imgTempat;

    @BindView(R.id.layoutInfoDetail)
    LinearLayout layoutInfoDetail;

    //@BindView(R.id.bottom_sheet)
    //NestedScrollView layoutBottomSheet;

    @BindView(R.id.imgTempat)
    ImageView img_tempat;

    @BindView(R.id.txt_tlp)
    TextView txt_tlp;

    @BindView(R.id.textJalan)
    TextView txt_jalan;

    @BindView(R.id.textNamaTempat1)
    TextView txt_nama_tempat;

    @BindView(R.id.textCategory)
    TextView txtType;

    @BindView(R.id.textJudul)
    TextView textJudul;

    @BindView(R.id.ratingBar)
    RatingBar ratingBar;

    @BindView(R.id.ratingBar_img)
    RatingBar ratingBar_img;

    @BindView(R.id.deskripsi_tempat)
    TextView deskripsi_tempat;

    @BindView(R.id.layout_deskriprsi)
    LinearLayout layout_deskriprsi;

    @BindView(R.id.btnGetDirection1)
    Button btnDirection1;

    @BindView(R.id.btnGetDirection2)
    Button btnDirection2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terdekat);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        progressBar = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        progressBar.setCancelable(true);
        progressBar.setMessage("Mencari Lokasi..");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.show();

        //navigation
        navigationView = findViewById(R.id.navigation_view);

        //Navigation
        navigationView.setCheckedItem(R.id.menu_none);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                if(menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);

                drawerLayout.closeDrawers();

                switch (menuItem.getItemId()){
                    // pilihan menu item
                    case R.id.navigation1:
                        openBeranda();
                        return true;
                    case R.id.navigation2:
                        openBookmark();
                        return true;
                    case R.id.navigation3:
                        //finish();
                        Intent intent = new Intent(TerdekatActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("Exit", true);
                        startActivity(intent);
                        finish();
                        return true;
                    case R.id.navigation4:
                        openAbout();
                        return true;
                    default:
                        Toast.makeText(getApplicationContext(),"Kesalahan Terjadi ",Toast.LENGTH_SHORT).show();
                        return true;
                }
            }
        });
        //Inisasi Drawer Layout dan ActionBarToggle
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer2);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.openDrawer, R.string.closeDrawer){
            @Override
            public void onDrawerClosed(View drawerView) {
                // Kode di sini akan merespons setelah drawer menutup disini kita biarkan kosong
                super.onDrawerClosed(drawerView);
            }
            @Override
            public void onDrawerOpened(View drawerView) {
                //  Kode di sini akan merespons setelah drawer terbuka disini kita biarkan kosong
                super.onDrawerOpened(drawerView);
            }
        };
        //Setting actionbarToggle drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        //memanggil synstate
        actionBarDrawerToggle.syncState();


        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorlayout);
        View bottomSheet = coordinatorLayout.findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehaviorGoogleMapsLike.from(bottomSheet);
        //behavior.setPeekHeight(300);
        behavior.setState(BottomSheetBehaviorGoogleMapsLike.STATE_HIDDEN);
        behavior.addBottomSheetCallback(new BottomSheetBehaviorGoogleMapsLike.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull final View bottomSheet, int newState) {
                bottomSheet.post(new Runnable() {
                    @Override
                    public void run() {
                        //workaround for the bottomsheet  bug
                        bottomSheet.requestLayout();
                        bottomSheet.invalidate();
                    }
                });

                switch (newState) {
                    case BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED:
                        //Log.d("bottomsheet-", "STATE_COLLAPSED");{
                        if (bottomSheetLayout.getVisibility() != View.VISIBLE) {
                            bottomSheetLayout.setVisibility(View.VISIBLE);
                            bottomSheetLayout.invalidate();
                        }
                        if (gapTambahan.getVisibility() != View.GONE) {
                            gapTambahan.setVisibility(View.GONE);
                            gapTambahan.invalidate();
                        }
                        break;
                    case BottomSheetBehaviorGoogleMapsLike.STATE_DRAGGING:
                        //Log.d("bottomsheet-", "STATE_DRAGGING");
                        /*if(bottomSheetLayout.getVisibility() != View.GONE) {
                            bottomSheetLayout.setVisibility(View.GONE);
                            bottomSheetLayout.invalidate();
                        }*/
                        if(gapTambahan.getVisibility() != View.GONE) {
                            gapTambahan.setVisibility(View.GONE);
                            gapTambahan.invalidate();
                        }
                        break;
                    case BottomSheetBehaviorGoogleMapsLike.STATE_EXPANDED:
                        //Log.d("bottomsheet-", "STATE_EXPANDED");
                        if(bottomSheetLayout.getVisibility() != View.GONE) {
                            bottomSheetLayout.setVisibility(View.GONE);
                            bottomSheetLayout.invalidate();
                        }
                        if(gapTambahan.getVisibility() != View.VISIBLE) {
                            gapTambahan.setVisibility(View.VISIBLE);
                            gapTambahan.invalidate();
                        }
                        break;
                    case BottomSheetBehaviorGoogleMapsLike.STATE_ANCHOR_POINT:
                        //Log.d("bottomsheet-", "STATE_ANCHOR_POINT");
                        if(bottomSheetLayout.getVisibility() != View.GONE) {
                            bottomSheetLayout.setVisibility(View.GONE);
                            bottomSheetLayout.invalidate();
                        }
                        if(gapTambahan.getVisibility() != View.GONE) {
                            gapTambahan.setVisibility(View.GONE);
                            gapTambahan.invalidate();
                        }

                        break;
                    case BottomSheetBehaviorGoogleMapsLike.STATE_HIDDEN:
                        Log.d("bottomsheet-", "STATE_HIDDEN");
                        /*if(bottomSheetLayout.getVisibility() != View.GONE) {
                            bottomSheetLayout.setVisibility(View.GONE);
                            bottomSheetLayout.invalidate();
                        }
                        if(gapTambahan.getVisibility() != View.GONE) {
                            gapTambahan.setVisibility(View.GONE);
                            gapTambahan.invalidate();
                        }*/
                        break;
                    default:
                        Log.d("bottomsheet-", "STATE_SETTLING");
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });

        MergedAppBarLayout mergedAppBarLayout = findViewById(R.id.mergedappbarlayout);
        mergedAppBarLayoutBehavior = MergedAppBarLayoutBehavior.from(mergedAppBarLayout);

        mergedAppBarLayoutBehavior.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED);
            }
        });

        //bottomSheetTextView = (TextView) bottomSheet.findViewById(R.id.bottom_sheet_title);
        /*ItemPagerAdapter adapter = new ItemPagerAdapter(this,mDrawables);
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(adapter);*/

        //behavior.setCollapsible(false);


        /*final ViewTreeObserver observer = bottomSheetLayout.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                behavior.setPeekHeight(bottomSheetLayout.getHeight() + 30);
                //observer.removeGlobalOnLayoutListener(this);
            }
        });
        layoutInfoDetail.setMinimumHeight(getWindowManager().getDefaultDisplay().getHeight());
        */

        //To check permissions above M as below it making issue and gives permission denied on samsung and other phones.
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.checkLocationPermission();
        }

        //To check google play service available
        if(!isGooglePlayServicesAvailable()){
            Toast.makeText(this,"Google Play Services not available.",Toast.LENGTH_LONG).show();
            finish();
        }else{
            // when the map is ready to be used.
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }

        mApiInterface = ApiClient.getGoogleAPIService();

        // BDG Navigation API
        BDGNavigationAPIInterface = BDGNavigationAPIClient.getBDGNavigationService();

        dbBookmark = new DBBookmark(getBaseContext());

        Intent iin= getIntent();
        Bundle b = iin.getExtras();

        if(b != null) {
            aksi = b.get("action").toString();
            if(aksi.equals("navigation")) {
                place_id = b.get("place_id").toString();
                nama_tempat = b.get("nama_tempat").toString();
                type_tempat = b.get("type_tempat").toString();
                rating_tempat = Float.parseFloat(b.get("rating_tempat").toString());
                alamat_tempat = b.get("alamat_tempat").toString();
            } else if(aksi.equals("terdekat")) {
                type = "terdekat";
            } else {
                type = b.get("type").toString();
                type2 = b.get("type").toString();
            }
        }


        this.data_result = new ArrayList<>();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // adds item to action bar
        getMenuInflater().inflate(R.menu.search_main, menu);

        // Get the search menu.
        MenuItem searchMenu = menu.findItem(R.id.action_search);

        // Get SearchView object.
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchMenu);

        // Get SearchView autocomplete object.
        searchAutoComplete = (SearchView.SearchAutoComplete)searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchAutoComplete.setDropDownBackgroundResource(android.R.color.background_light);
        searchAutoComplete.setShadowLayer(0.2f, 0.2f, 0.2f, android.R.color.darker_gray);
        // Create a new ArrayAdapter and add data to search auto complete object.
        //String dataArr[] = {"Apple" , "Amazon" , "Amd", "Microsoft", "Microwave", "MicroNews", "Intel", "Intelligence"};
        //ArrayAdapter<String> newsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, dataArr);

        // Listen to search view item on click event.
        searchAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int itemIndex, long id) {
                Result result = (Result) adapterView.getItemAtPosition(itemIndex);
                searchAutoComplete.setText("" + result.getName());
                setDataBottomSheet(result);
                navigasi(result);
            }
        });

        // Below event is triggered when submit search query.
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                AlertDialog alertDialog = new AlertDialog.Builder(TerdekatActivity.this).create();
                alertDialog.setMessage("Search keyword is " + query);
                alertDialog.show();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }



    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result, 0).show();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json));
        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                googleMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            googleMap.setMyLocationEnabled(true);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 99);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 99);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 99: { //MY_PERMISSIONS_REQUEST_LOCATION
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        googleMap.setMyLocationEnabled(true);
                    }

                } else {
                    Toast.makeText(this, "Location Permission has been denied, can not search the places you want.", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this,"Could not connect google api",Toast.LENGTH_LONG).show();
    }

    protected void startLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if(location != null){
            this.location = location;
            if(aksi != null) {
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(16));
                if(aksi.equals("terdekat")) {
                    cariTempat("shopping_mall");
                    cariTempat("cafe");
                    cariTempat("museum");
                    cariTempat("library");
                    cariTempat("lodging");
                    cariTempat("park");
                    cariTempat("restaurant");
                    cariTempat("gym");
                } else if(aksi.equals("kategori")) {
                    googleMap.clear();
                    cariTempat(type);
                    type = null;
                } else if(aksi.equals("navigation")) {
                    if(place_id != null) {
                        Result j = new Result();
                        j.setPlaceId(place_id);
                        j.setName(nama_tempat);
                        j.setType(type_tempat);
                        j.setRating(rating_tempat);
                        j.setVicinity(alamat_tempat);
                        obj = j;
                        setDataBottomSheet(j);
                        navigasi(j);
                        //behavior.setState(BottomSheetBehaviorGoogleMapsLike.STATE_HIDDEN);
                        place_id = null;
                    }
                }
                aksi = null;
            }
            /*if(!btnHospitalFind.isEnabled()){
                LatLng latLng = new LatLng(location.getLatitude(),location.getLongitude());
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));

                if(place_id != null) {
                    Log.d("test", place_id);
                    Result j = new Result();
                    j.setPlaceId(place_id);
                    j.setName(nama_tempat);
                    //test_2(j);
                    place_id = null;
                }
            }*/
        }
    }

    public void cariTempat(final String placeType){
        Call<NearByApiResponse> call = mApiInterface.getNearbyPlaces(placeType, location.getLatitude() + "," + location.getLongitude(), PROXIMITY_RADIUS);

        call.enqueue(new Callback<NearByApiResponse>() {
            @Override
            public void onResponse(Call<NearByApiResponse> call, Response<NearByApiResponse> response) {
                try {

                    // This loop will go through all the results and add marker on each location.
                    for (int i = 0; i < response.body().getResults().size(); i++) {
                        Double lat = response.body().getResults().get(i).getGeometry().getLocation().getLat();
                        Double lng = response.body().getResults().get(i).getGeometry().getLocation().getLng();
                        String placeName = response.body().getResults().get(i).getName();
                        String placeid = response.body().getResults().get(i).getPlaceId();
                        String vicinity = response.body().getResults().get(i).getVicinity();
                        response.body().getResults().get(i).setType(placeType);
                        data_result.add(response.body().getResults().get(i));

                        MarkerOptions markerOptions = new MarkerOptions();
                        LatLng latLng = new LatLng(lat, lng);

                        // Location of Marker on Map
                        markerOptions.position(latLng);

                        // Title for Marker
                        markerOptions.title(placeName);

                        // Color or drawable for marker
                        if(placeType.equals("shopping_mall")) {
                            markerOptions.icon(bitmapDescriptorFromVector(getApplication(), R.drawable.mrk_mall));
                        } else if(placeType.equals("cafe")) {
                            markerOptions.icon(bitmapDescriptorFromVector(getApplication(), R.drawable.mrk_kafe));
                        } else if(placeType.equals("museum")) {
                            markerOptions.icon(bitmapDescriptorFromVector(getApplication(), R.drawable.mrk_museum));
                        } else if(placeType.equals("lodging")) {
                            markerOptions.icon(bitmapDescriptorFromVector(getApplication(), R.drawable.mrk_hotel));
                        } else if(placeType.equals("library")) {
                            markerOptions.icon(bitmapDescriptorFromVector(getApplication(), R.drawable.mrk_perpus));
                        } else if(placeType.equals("park")) {
                            markerOptions.icon(bitmapDescriptorFromVector(getApplication(), R.drawable.mrk_taman));
                        } else if(placeType.equals("restaurant")) {
                            markerOptions.icon(bitmapDescriptorFromVector(getApplication(), R.drawable.mrk_resto));
                        } else if(placeType.equals("gym")) {
                            markerOptions.icon(bitmapDescriptorFromVector(getApplication(), R.drawable.mrk_gym));
                        }

                        // add marker
                        Marker m = googleMap.addMarker(markerOptions);
                        m.setTag(response.body().getResults().get(i));
                        // move map camera
                        LatLng latLng2 = new LatLng(location.getLatitude(), location.getLongitude());
                        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng2));
                        googleMap.animateCamera(CameraUpdateFactory.zoomTo(16));
                        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                            @Override
                            public boolean onMarkerClick(Marker marker) {
                                if(marker.getTag() != null) {
                                    obj = ((Result) marker.getTag());

                                    // disable button direction
                                    btnDirection1.setEnabled(false);
                                    btnDirection2.setEnabled(false);

                                    //test(obj);
                                    //Button btnBookmark = (Button) findViewById(R.id.btnBookmark);
                                    //btnBookmark.setEnabled(true);
                                    behavior.setState(BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED);
                                    setDataBottomSheet(obj);
                                }
                                return false;
                            }
                        });
                        googleMap.setOnInfoWindowCloseListener(new GoogleMap.OnInfoWindowCloseListener() {
                            @Override
                            public void onInfoWindowClose(Marker marker) {
                                behavior.setState(BottomSheetBehaviorGoogleMapsLike.STATE_HIDDEN);
                            }
                        });
                    }
                    progressBar.hide();
                    searchAdapter = new SearchPlaceAdapter(TerdekatActivity.this, data_result);
                    searchAdapter.notifyDataSetChanged();
                    searchAutoComplete.setAdapter(searchAdapter);
                } catch (Exception e) {
                    Log.d("onResponse", "There is an error");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<NearByApiResponse> call, Throwable t) {
                Log.d("onFailure", t.toString());
                t.printStackTrace();
                //PROXIMITY_RADIUS += 10000;
            }
        });
    }

    void getPhoto(String photoreference) {
        Call<ResponseBody> call = mApiInterface.getImage(photoreference);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        // display the image data in a ImageView or save it
                        Bitmap bm = BitmapFactory.decodeStream(response.body().byteStream());
                        img_tempat.setImageBitmap(bm);
                        img_tempat.setScaleType(ImageView.ScaleType.FIT_XY);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("onFailure", t.toString());
                t.printStackTrace();
            }
        });
    }

    void getDetail(String pid) {
        Call<DetailResponse> call = mApiInterface.getDetail(pid);

        call.enqueue(new Callback<DetailResponse>() {
            @Override
            public void onResponse(Call<DetailResponse> call, Response<DetailResponse> response) {
                String jam_buka = "";
                if (response.code() == 200) {
                    if (response.body() != null) {
                        txt_tlp.setText(response.body().getResult().getFormatted_phone_number());
                        //txt_buka.setText(String.join("\n", response.body().getResult().getOpeningHours().getWeekdayText()));
                        /*TextView txtJamBuka = (TextView) findViewById(R.id.textJam);
                        // initialize listview
                        if(response.body().getResult().getOpeningHours() != null) {
                            //lv.setAdapter(new OpeningHoursAdapter(TerdekatActivity.this, response.body().getResult().getOpeningHours().getWeekdayText()));
                            for (Iterator<String> i = response.body().getResult().getOpeningHours().getWeekdayText().iterator(); i.hasNext();) {
                                String item = i.next();
                                jam_buka += item + "\n";
                            }
                        }
                        txtJamBuka.setText(jam_buka
                                .replace("Monday:", "Monday\t\t\t\t: ")
                                .replace("Tuesday:", "Tuesday\t\t\t: ")
                                .replace("Wednesday:", "Wednesday\t\t: ")
                                .replace("Thursday:", "Thursday\t\t\t: ")
                                .replace("Friday:", "Friday\t\t\t\t\t: ")
                                .replace("Saturday:", "Saturday\t\t\t: ")
                                .replace("Sunday:", "Sunday\t\t\t\t: ")
                        );*/
                        ListView lv = (ListView) findViewById(R.id.list_opening);
                        // initialize listview
                        if(response.body().getResult().getOpeningHours() != null) {
                            lv.setAdapter(new OpeningHoursAdapter(TerdekatActivity.this, response.body().getResult().getOpeningHours().getWeekdayText()));
                        }

                        if(response.body().getResult().getPhotos().size() > 0) {
                            //img_tempat.setVisibility(View.VISIBLE);
                            getPhoto(response.body().getResult().getPhotos().get(0).getPhotoReference());
                        } else {
                            //img_tempat.setVisibility(View.GONE);
                            img_tempat.setImageDrawable(null);
                        }
                        behavior.setState(BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED);
                    }
                }
            }

            @Override
            public void onFailure(Call<DetailResponse> call, Throwable t) {
                Log.d("onFailure", t.toString());
                t.printStackTrace();
            }
        });

        Call<BDGNavigationApiResponse> call2 = BDGNavigationAPIInterface.getDescription(pid);

        call2.enqueue(new Callback<BDGNavigationApiResponse>() {
            @Override
            public void onResponse(Call<BDGNavigationApiResponse> call, Response<BDGNavigationApiResponse> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if(!response.body().getMessage().equals("")) {
                            deskripsi_tempat.setText(response.body().getMessage());
                            layout_deskriprsi.setVisibility(View.VISIBLE);
                        } else {
                            layout_deskriprsi.setVisibility(View.GONE);
                            deskripsi_tempat.setText("");
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<BDGNavigationApiResponse> call, Throwable t) {
                Log.d("onFailure", t.toString());
                t.printStackTrace();
            }
        });
    }

    public void getRute(Result obj){
        Call<DirectionApiResponse> getDirection = mApiInterface.getDirection(location.getLatitude() + "," + location.getLongitude(), "place_id:" + obj.getPlaceId().toString(), "driving");

        final String namaTempat = obj.getName();
        getDirection.enqueue(new Callback<DirectionApiResponse>() {
            @Override
            public void onResponse(Call<DirectionApiResponse> call, Response<DirectionApiResponse> response) {
                // Get result Repo from response.body()

                DirectionApiResponse answer = response.body();

                if (!answer.getRoutes().isEmpty()) {
                    DirectionAnswer = answer;

                /*
                    googleMap.clear();
                    String encodedPoints = answer.getRoutes().get(0).getOverview_polyline().getPoints();
                    latLngs = PolyUtil.decode(encodedPoints);

                    MarkerOptions markerOptions = new MarkerOptions();

                    LatLng latLng = new LatLng(latLngs.get(latLngs.size() - 1).latitude, latLngs.get(latLngs.size() - 1).longitude);
                    // Location of Marker on Map
                    markerOptions.position(latLng);
                    // Title for Marker
                    markerOptions.title(namaTempat);
                    // Color or drawable for marker
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                    // add marker
                    Marker m = googleMap.addMarker(markerOptions);

                    //Log.d("MYTAG", "origin " + mUserLatLng.latitude + "," + mUserLatLng.longitude + " _ " + "destination " + latLng.latitude + "," + latLng.longitude );
                    if(polyline != null) {
                        polyline.remove();
                    }
//Add the polyline to map
                    LatLng latLng2 = new LatLng(location.getLatitude(), location.getLongitude());
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng2));
                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(13));
                    rectOptions = new PolylineOptions()
                            .addAll(latLngs)
                            .width(5)
                            .color(Color.rgb(255,64,129));
                    polyline = googleMap.addPolyline(rectOptions);
//See BudgetTrackDetailsFragment

//display summaring Toast*/
                    if (answer.getRoutes().get(0).getLegs().size() == 1)  //One leg trip, directly use data as string in UI
                    {

                        TextView txt = (TextView) findViewById(R.id.textJarak);
                        txt.setText(answer.getRoutes().get(0).getLegs().get(0).getDistance().getText());
                        //mTextDistance.setText(answer.routes.get(0).legs.get(0).distance.text);
                        //mTextDuration.setText(answer.routes.get(0).legs.get(0).duration.text);
                        //Toast.makeText(getActivity().getApplicationContext(), answer.routes.get(0).legs.get(0).distance.text + "  " + answer.routes.get(0).legs.get(0).duration.text, Toast.LENGTH_LONG).show();
                        //mButtonGo.setEnabled(true);

                        btnDirection1.setEnabled(true);
                        btnDirection2.setEnabled(true);
                    }
                    //behavior.setState(BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED);
                }
                else
                {
                    Log.d("err", "No route!");
                }
            }

            @Override
            public void onFailure(Call<DirectionApiResponse> call, Throwable t) {
                Log.d("onFailure", t.toString());
                t.printStackTrace();
            }
        });
    }

    public void navigasi(Result obj){
        Call<DirectionApiResponse> getDirection = mApiInterface.getDirection(location.getLatitude() + "," + location.getLongitude(), "place_id:" + obj.getPlaceId().toString(), "driving");

        final String namaTempat = obj.getName();
        getDirection.enqueue(new Callback<DirectionApiResponse>() {
            @Override
            public void onResponse(Call<DirectionApiResponse> call, Response<DirectionApiResponse> response) {
                // Get result Repo from response.body()

                DirectionApiResponse answer = response.body();

                if (!answer.getRoutes().isEmpty()) {
                    DirectionAnswer = answer;
                    googleMap.clear();
                    String encodedPoints = answer.getRoutes().get(0).getOverview_polyline().getPoints();
                    latLngs = PolyUtil.decode(encodedPoints);

                    MarkerOptions markerOptions = new MarkerOptions();

                    LatLng latLng = new LatLng(latLngs.get(latLngs.size() - 1).latitude, latLngs.get(latLngs.size() - 1).longitude);
                    // Location of Marker on Map
                    markerOptions.position(latLng);
                    // Title for Marker
                    markerOptions.title(namaTempat);
                    // Color or drawable for marker
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                    // add marker
                    Marker m = googleMap.addMarker(markerOptions);

                    //Log.d("MYTAG", "origin " + mUserLatLng.latitude + "," + mUserLatLng.longitude + " _ " + "destination " + latLng.latitude + "," + latLng.longitude );
                    if(polyline != null) {
                        polyline.remove();
                    }
//Add the polyline to map
                    LatLng latLng2 = new LatLng(location.getLatitude(), location.getLongitude());
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng2));
                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(17));
                    rectOptions = new PolylineOptions()
                            .addAll(latLngs)
                            .width(5)
                            .color(Color.rgb(255,64,129));
                    polyline = googleMap.addPolyline(rectOptions);
//See BudgetTrackDetailsFragment

//display summaring Toast
                    if (answer.getRoutes().get(0).getLegs().size() == 1)  //One leg trip, directly use data as string in UI
                    {

                        TextView txt = (TextView) findViewById(R.id.textJarak);
                        txt.setText(answer.getRoutes().get(0).getLegs().get(0).getDistance().getText());
                        //mTextDistance.setText(answer.routes.get(0).legs.get(0).distance.text);
                        //mTextDuration.setText(answer.routes.get(0).legs.get(0).duration.text);
                        //Toast.makeText(getActivity().getApplicationContext(), answer.routes.get(0).legs.get(0).distance.text + "  " + answer.routes.get(0).legs.get(0).duration.text, Toast.LENGTH_LONG).show();
                        //mButtonGo.setEnabled(true);
                    }
                    //behavior.setState(BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED);
                }
                else
                {
                    Log.d("err", "No route!");
                }
                progressBar.hide();

            }

            @Override
            public void onFailure(Call<DirectionApiResponse> call, Throwable t) {
                Log.d("onFailure", t.toString());
                t.printStackTrace();
            }
        });
    }

    public void showRute(Result obj) {
        final String namaTempat = obj.getName();
        if (!DirectionAnswer.getRoutes().isEmpty()) {
            googleMap.clear();
            String encodedPoints = DirectionAnswer.getRoutes().get(0).getOverview_polyline().getPoints();
            latLngs = PolyUtil.decode(encodedPoints);

            MarkerOptions markerOptions = new MarkerOptions();

            LatLng latLng = new LatLng(latLngs.get(latLngs.size() - 1).latitude, latLngs.get(latLngs.size() - 1).longitude);
            // Location of Marker on Map
            markerOptions.position(latLng);
            // Title for Marker
            markerOptions.title(namaTempat);
            // Color or drawable for marker
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
            // add marker
            Marker m = googleMap.addMarker(markerOptions);

            //Log.d("MYTAG", "origin " + mUserLatLng.latitude + "," + mUserLatLng.longitude + " _ " + "destination " + latLng.latitude + "," + latLng.longitude );
            if(polyline != null) {
                polyline.remove();
            }
    //Add the polyline to map
            LatLng latLng2 = new LatLng(location.getLatitude(), location.getLongitude());
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng2));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(17));
            rectOptions = new PolylineOptions()
                    .addAll(latLngs)
                    .width(5)
                    .color(Color.rgb(255,64,129));
            polyline = googleMap.addPolyline(rectOptions);
    //See BudgetTrackDetailsFragment

            behavior.setState(BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED);
            setDataBottomSheet(obj);
        }
        else
        {
            Log.d("err", "No route!");
        }
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @OnClick({R.id.btnBookmark, R.id.btnBookmark2})
    void btn_bookmark() {
        SQLiteDatabase create = dbBookmark.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("place_id", obj.getPlaceId());
        values.put("nama", obj.getName());
        values.put("category", obj.getType());
        if(obj.getRating() != null) {
            values.put("rating", obj.getRating());
        }
        values.put("vicinity", obj.getVicinity());

        create.insert("Bookmark", null, values);
        Toast.makeText(getApplication(), "Lokasi berhasil dibookmark", Toast.LENGTH_LONG).show();
    }

    @OnClick({R.id.btnGetDirection1, R.id.btnGetDirection2})
    void btn_navigation1() {
        showRute(obj);
    }

    @OnClick(R.id.btnShare)
    void share() {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");

        String uri = "http://maps.google.com/maps?daddr=" + obj.getGeometry().getLocation().getLat().toString() +"," + obj.getGeometry().getLocation().getLng().toString();
        String text = "@" + obj.getName() + " - " + obj.getVicinity() + " " + uri;

        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, obj.getName());
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, text);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    private void setDataBottomSheet(Result obj) {
        txt_nama_tempat.setText(obj.getName());
        txt_jalan.setText(obj.getVicinity());
        txtType.setText(typeTempat(obj.getType()));
        textJudul.setText(obj.getName());
        mergedAppBarLayoutBehavior.setToolbarTitle(obj.getName());
        if(obj.getRating() != null) {
            ratingBar.setRating(obj.getRating());
            ratingBar.setVisibility(View.VISIBLE);
            ratingBar_img.setRating(obj.getRating());
            ratingBar_img.setVisibility(View.VISIBLE);
        } else {
            ratingBar.setVisibility(View.INVISIBLE);
            ratingBar_img.setVisibility(View.INVISIBLE);
        }

        /*if(obj.getPhotos().size() > 0) {
            //img_tempat.setVisibility(View.VISIBLE);
            getPhoto(obj.getPhotos().get(0).getPhotoReference());
        } else {
            //img_tempat.setVisibility(View.GONE);
            img_tempat.setImageDrawable(null);
        }*/
        getDetail(obj.getPlaceId());
        getRute(obj);
        //sheetBehavior.setPeekHeight(findViewById(R.id.judul).getHeight());
        //behavior.setState(BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED);
    }

    public void openBeranda(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void openBookmark(){
        Intent intent = new Intent(this, BookmarkActivity.class);
        startActivity(intent);
    }

    private String typeTempat(String placeType) {
        String ret = "";
        if(placeType.equals("shopping_mall")) {
            ret = "Mall";
        } else if(placeType.equals("cafe")) {
            ret = "Cafe";
        } else if(placeType.equals("museum")) {
            ret = "Museum";
        } else if(placeType.equals("lodging")) {
            ret = "Hotel";
        } else if(placeType.equals("library")) {
            ret = "Perpustakaan";
        } else if(placeType.equals("park")) {
            ret = "Taman";
        } else if(placeType.equals("restaurant")) {
            ret = "Restoran";
        } else if(placeType.equals("gym")) {
            ret = "Gym";
        }
        return ret;
    }

    @OnClick(R.id.buttonSwapUp)
    void btnimgBtnSwapUp() {
        if(behavior.getState() == BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED) {
            behavior.setState(BottomSheetBehaviorGoogleMapsLike.STATE_ANCHOR_POINT);
        }
    }

    public void openAbout() {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }
}
