package id.ac.unikom.bdgnavigation.model;

import com.google.gson.annotations.Expose;

import java.util.List;

public class Route {
    //@SerializedName("legs")
    @Expose
    private List<Leg> legs;

    //@SerializedName("overview_polyline")
    @Expose
    private OverviewPolyline overview_polyline;

    public List<Leg> getLegs() {
        return legs;
    }

    public void setLegs(List<Leg> legs) {
        this.legs = legs;
    }

    public OverviewPolyline getOverview_polyline() {
        return overview_polyline;
    }

    public void setOverview_polyline(OverviewPolyline overview_polyline) {
        this.overview_polyline = overview_polyline;
    }
}
