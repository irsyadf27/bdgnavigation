package id.ac.unikom.bdgnavigation;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.ac.unikom.bdgnavigation.database.DBBookmark;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.bottom_sheet_main)
    FrameLayout layoutBottomSheet;

    @BindView(R.id.buttonSwapUpLayout)
    FrameLayout buttonSwapUpLayout;

    @BindView(R.id.btnLibrary)
    Button btnLibrary;

    @BindView(R.id.imgBtnSwapUp)
    ImageView imgBtnSwapUp;

    @BindView(R.id.navigation_view)
    NavigationView navigationView;

    @BindView(R.id.drawer)
    DrawerLayout drawerLayout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    BottomSheetBehavior sheetBehavior;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        toolbar.setTitle("BDGNavigation");

        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        buttonSwapUpLayout.setZ(10);

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        btnLibrary.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.ic_other) , null, null);
                        btnLibrary.setText("Other");
                        imgBtnSwapUp.setImageResource(R.drawable.ic_arrow_up);
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        btnLibrary.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.ic_library) , null, null);
                        btnLibrary.setText("Library");
                        imgBtnSwapUp.setImageResource(R.drawable.ic_arrow_down);
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });

        //Navigation
        navigationView.setCheckedItem(R.id.navigation1);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                if(menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);

                drawerLayout.closeDrawers();

                switch (menuItem.getItemId()){
                    // pilihan menu item
                    case R.id.navigation1:
                        //Toast.makeText(getApplicationContext(), "Beranda Telah Dipilih", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.navigation2:
                        openBookmark();
                        return true;
                    case R.id.navigation3:
                        finish();
                        return true;
                    case R.id.navigation4:
                        openAbout();
                        return true;
                    default:
                        Toast.makeText(getApplicationContext(),"Kesalahan Terjadi ",Toast.LENGTH_SHORT).show();
                        return true;
                }
            }
        });

        //Inisasi Drawer Layout dan ActionBarToggle
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.openDrawer, R.string.closeDrawer){
            @Override
            public void onDrawerClosed(View drawerView) {
                // Kode di sini akan merespons setelah drawer menutup disini kita biarkan kosong
                super.onDrawerClosed(drawerView);
            }
            @Override
            public void onDrawerOpened(View drawerView) {
                //  Kode di sini akan merespons setelah drawer terbuka disini kita biarkan kosong
                super.onDrawerOpened(drawerView);
            }
        };
        //Setting actionbarToggle drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        //memanggil synstate
        actionBarDrawerToggle.syncState();

        // buat databasae
        DBBookmark dbBookmark = new DBBookmark(getBaseContext());
        SQLiteDatabase create = dbBookmark.getWritableDatabase();
    }

    public void openBookmark(){
        Intent intent = new Intent(this, BookmarkActivity.class);
        startActivity(intent);
    }

    public void openAbout() {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    public void openTerdekat(String type){
        Intent intent = new Intent(this, TerdekatActivity.class);
        if(type.equals("all")) {
            intent.putExtra("action", "terdekat");
            intent.putExtra("type", type);
        } else {
            intent.putExtra("action", "kategori");
            intent.putExtra("type", type);
        }
        startActivity(intent);
    }

    @OnClick(R.id.btnLibrary)
    void btnLibrary(Button btn) {
        if(btn.getText().equals("Other")) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            openTerdekat("library");
        }
    }

    @OnClick(R.id.imgBtnSwapUp)
    void btnimgBtnSwapUp() {
        if(sheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    //fungsi button
    @OnClick(R.id.btn_terdekat)
    void btn_terdekat() {
        openTerdekat("all");
    }

    @OnClick(R.id.btn_mall)
    void btn_mall() {
        openTerdekat("shopping_mall");
    }

    @OnClick(R.id.btn_kafe)
    void btn_kafe() {
        openTerdekat("cafe");
    }

    @OnClick(R.id.btn_museum)
    void btn_museum() {
        openTerdekat("museum");
    }

    @OnClick(R.id.btn_hotel)
    void btn_hotel() {
        openTerdekat("lodging");
    }

    @OnClick(R.id.btn_taman)
    void btn_taman() {
        openTerdekat("park");
    }

    @OnClick(R.id.btn_restoran)
    void btn_restoran() {
        openTerdekat("restaurant");
    }

    @OnClick(R.id.btn_gym)
    void btn_gym() {
        openTerdekat("gym");
    }
}
