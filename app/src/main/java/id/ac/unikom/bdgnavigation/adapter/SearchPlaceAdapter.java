package id.ac.unikom.bdgnavigation.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.ac.unikom.bdgnavigation.R;
import id.ac.unikom.bdgnavigation.model.Result;

public class SearchPlaceAdapter extends ArrayAdapter<Result> {
    private List<Result> listResult;

    public SearchPlaceAdapter(@NonNull Context context, @NonNull List<Result> lr) {
        super(context, 0, lr);
        this.listResult = new ArrayList<>(lr);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return resultFilter;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.row_search, parent, false
            );
        }

        TextView txtNama = convertView.findViewById(R.id.row_search_txtNamaTempat);
        TextView txtCategory = convertView.findViewById(R.id.row_search_txtCategory);

        Result resultItem = getItem(position);

        if (resultItem != null) {
            txtNama.setText(resultItem.getName());
            txtCategory.setText(resultItem.getType());
        }

        return convertView;
    }

    private Filter resultFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            List<Result> suggestions = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                suggestions.addAll(listResult);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                Log.d("test", String.valueOf(listResult.size()));
                for (Result item : listResult) {
                    if (item.getName().toLowerCase().contains(filterPattern)) {
                        suggestions.add(item);
                    }
                }
            }

            results.values = suggestions;
            results.count = suggestions.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            addAll((List) results.values);
            notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((Result) resultValue).getName();
        }
    };
}

