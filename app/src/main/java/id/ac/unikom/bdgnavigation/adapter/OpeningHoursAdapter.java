package id.ac.unikom.bdgnavigation.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import id.ac.unikom.bdgnavigation.R;

public class OpeningHoursAdapter extends ArrayAdapter {

    List<String> OpeningHours;

    public OpeningHoursAdapter(Context context, List<String> list)
    {
        super(context,0,list);
        OpeningHours = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        // Get the data item for this position
        String openinghour = getItem(position).toString();
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_opening_hours, parent, false);
        }
        // Lookup view for data population
        TextView hari_buka = (TextView) convertView.findViewById(R.id.hari_buka);
        TextView jam_buka = (TextView) convertView.findViewById(R.id.jam_buka);
        // Populate the data into the template view using the data object
        hari_buka.setText(openinghour.split(": ")[0].trim());
        jam_buka.setText(openinghour.split(": ")[1].trim());
        // Return the completed view to render on screen
        return convertView;

    }

    static class ViewHolder
    {

        TextView tv;
    }
}