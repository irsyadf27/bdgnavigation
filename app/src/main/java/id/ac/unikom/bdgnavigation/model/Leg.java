package id.ac.unikom.bdgnavigation.model;

import com.google.gson.annotations.Expose;

public class Leg {
    //@SerializedName("distance")
    @Expose
    private LegDistance distance;

    //@SerializedName("duration")
    @Expose
    private LegDuration duration;

    public LegDistance getDistance() {
        return distance;
    }

    public void setDistance(LegDistance distance) {
        this.distance = distance;
    }

    public LegDuration getDuration() {
        return duration;
    }

    public void setDuration(LegDuration duration) {
        this.duration = duration;
    }
}
