package id.ac.unikom.bdgnavigation.network.api;

import id.ac.unikom.bdgnavigation.model.DetailResponse;
import id.ac.unikom.bdgnavigation.model.DirectionApiResponse;
import id.ac.unikom.bdgnavigation.model.NearByApiResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GoogleAPI {
    @GET("api/place/nearbysearch/json?key=AIzaSyDmDnyy-R-UGcWvH2NWkZycwLF8NFnHqec&sensor=true")
    Call<NearByApiResponse> getNearbyPlaces(@Query("type") String type, @Query("location") String location, @Query("radius") int radius);

    @GET("api/place/nearbysearch/json?key=AIzaSyDmDnyy-R-UGcWvH2NWkZycwLF8NFnHqec&sensor=true")
    Call<NearByApiResponse> searchNearbyPlaces(@Query("type") String type, @Query("location") String location, @Query("radius") int radius, @Query("name") String name);

    @GET("api/directions/json?key=AIzaSyDmDnyy-R-UGcWvH2NWkZycwLF8NFnHqec&sensor=true")
    Call<DirectionApiResponse> getDirection(@Query("origin") String origin, @Query("destination") String destination, @Query("mode") String mode);

    @GET("api/place/details/json?fields=photo,type,vicinity,opening_hours,name,rating,formatted_phone_number&key=AIzaSyDmDnyy-R-UGcWvH2NWkZycwLF8NFnHqec")
    Call<DetailResponse> getDetail(@Query("placeid") String place_id);

    @GET("api/place/photo?maxwidth=1024&key=AIzaSyDmDnyy-R-UGcWvH2NWkZycwLF8NFnHqec")
    Call<ResponseBody> getImage(@Query("photoreference") String photoreference);
}
