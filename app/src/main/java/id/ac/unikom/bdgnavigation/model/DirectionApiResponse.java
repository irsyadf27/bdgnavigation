package id.ac.unikom.bdgnavigation.model;

import com.google.gson.annotations.Expose;

import java.util.List;

public class DirectionApiResponse {
    //@SerializedName("routes")
    @Expose
    private List<Route> routes;

    //@SerializedName("status")
    @Expose
    private String status;

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
