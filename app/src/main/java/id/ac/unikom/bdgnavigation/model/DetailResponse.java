package id.ac.unikom.bdgnavigation.model;

import com.google.gson.annotations.Expose;

public class DetailResponse {
    //@SerializedName("routes")
    @Expose
    private ResultDetail result;

    //@SerializedName("status")
    @Expose
    private String status;

    public ResultDetail getResult() {
        return result;
    }

    public void setResult(ResultDetail result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
